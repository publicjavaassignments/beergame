public class Dranker extends Player {
    private final int playerID = 69420;
    private int opponent;
    private int moveNumber = 0;

    public Dranker(MoveBeer moveBeerCallBack) {
        super(moveBeerCallBack);
    }

    @Override
    public void run() {
        try {
            // Move 1
            moveBeerCallBack.move(PushAction.pull, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 2
            moveBeerCallBack.move(PushAction.pull, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 3
            moveBeerCallBack.move(PushAction.pull, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 4
            opponent = moveBeerCallBack.look(LookAction.top, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 5
            if (opponent > 0) {
                moveBeerCallBack.move(PushAction.pull, playerID);
                System.out.println("Executed move "+ moveNumber++);
                Thread.sleep(1000);
            } else {
                // Gør intet lmao lol
                System.out.println("Executed move "+ moveNumber++);
                Thread.sleep(1000);
            }

            // Move 6
            moveBeerCallBack.move(PushAction.pull, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 7
            moveBeerCallBack.move(PushAction.pull, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 8
            moveBeerCallBack.move(PushAction.pull, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 9
            opponent = moveBeerCallBack.look(LookAction.top, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 10
            if (opponent > 0) {
                moveBeerCallBack.move(PushAction.pull, playerID);
                System.out.println("Executed move "+ moveNumber++);
                Thread.sleep(1000);
            } else {
                // Gør intet lmao lol
                System.out.println("Executed move "+ moveNumber++);
                Thread.sleep(1000);
            }

            // Move 11
            moveBeerCallBack.move(PushAction.pull, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 12
            moveBeerCallBack.move(PushAction.pull, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 13
            moveBeerCallBack.move(PushAction.pull, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 14
            opponent = moveBeerCallBack.look(LookAction.top, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 15
            if (opponent > 0) {
                moveBeerCallBack.move(PushAction.pull, playerID);
                System.out.println("Executed move "+ moveNumber++);
                Thread.sleep(1000);
            } else {
                // Gør intet lmao lol
                System.out.println("Executed move "+ moveNumber++);
                Thread.sleep(1000);
            }

            // Move 16
            moveBeerCallBack.move(PushAction.pull, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 17
            moveBeerCallBack.move(PushAction.pull, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 18
            moveBeerCallBack.move(PushAction.pull, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 19
            opponent = moveBeerCallBack.look(LookAction.top, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 20
            if (opponent > 0) {
                moveBeerCallBack.move(PushAction.pull, playerID);
                System.out.println("Executed move "+ moveNumber++);
                Thread.sleep(1000);
            } else {
                // Gør intet lmao lol
                System.out.println("Executed move "+ moveNumber++);
                Thread.sleep(1000);
            }

            // Move 21
            moveBeerCallBack.move(PushAction.pull, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 22
            moveBeerCallBack.move(PushAction.pull, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 23
            moveBeerCallBack.move(PushAction.pull, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 24
            opponent = moveBeerCallBack.look(LookAction.top, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 25
            if (opponent > 0) {
                moveBeerCallBack.move(PushAction.pull, playerID);
                System.out.println("Executed move "+ moveNumber++);
                Thread.sleep(1000);
            } else {
                // Gør intet lmao lol
                System.out.println("Executed move "+ moveNumber++);
                Thread.sleep(1000);
            }

            // Move 26
            moveBeerCallBack.move(PushAction.pull, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 27
            moveBeerCallBack.move(PushAction.pull, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 28
            moveBeerCallBack.move(PushAction.pull, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 29
            opponent = moveBeerCallBack.look(LookAction.top, playerID);
            System.out.println("Executed move "+ moveNumber++);
            Thread.sleep(1000);

            // Move 30
            int myBeers = moveBeerCallBack.look(LookAction.bottom, playerID);
            if (myBeers == 3) {
                System.out.println("Executed move "+ moveNumber++);
                System.out.println("Jubiiii hvor er det bare godt så sejt mander");
            } else {
                System.out.println("Executed move "+ moveNumber++);
                System.out.println("øvøvøvøvøvøvøøvøvøvøøvv");
            }


        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
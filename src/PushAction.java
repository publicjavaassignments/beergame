/**
 * Created by fha on 09-04-2018.
 */
public enum PushAction {
    moveUp, moveDown, push, pull
}

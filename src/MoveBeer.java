/**
 * Created by fha on 09-04-2018.
 */
public interface MoveBeer {
    void move(PushAction doPush, int id);

    int look(LookAction doLook, int id);

}
